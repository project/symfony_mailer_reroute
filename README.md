# Description

Provides rerouting options for the
[Symfony Mailer](https://www.drupal.org/project/symfony_mailer) module.

This functionality somewhat similar to the
[Reroute mail](https://www.drupal.org/project/reroute_mail) module, but is
specific for Symfony Mailer, and uses this module's Policy functionality, to
decide which email message(s) to reroute.

# Configuration

* First, decide if you want to globally enable rerouting or not. Go to
  /admin/config/system/mailer/reroute to enable routing functionality. If the
  checkbox is unchecked, no rerouting at all will happen.
* Optionally, you can override this setting per environment in your settings.php
  or settings.local.php file (e.g. to only enable rerouting on your dev site):
  `$config['symfony_mailer_reroute.settings']['enable'] = TRUE;`
* Go to the policy settings page of Symfony Mailer (/admin/config/system/mailer)
* Edit the policy for which you wish to reroute emails (could be a specific mail
  , or the `*All*` policy). Add the "Reroute email (if enabled)" element to your
  policy.
* Define one or more addresses to reroute to within the policy settings.
