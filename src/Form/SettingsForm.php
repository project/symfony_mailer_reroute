<?php

namespace Drupal\symfony_mailer_reroute\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements a settings form for Symfony Mail Reroute configuration.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'symfony_mailer_reroute_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['symfony_mailer_reroute.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable rerouting'),
      '#default_value' => $this->config('symfony_mailer_reroute.settings')->get('enable'),
      '#description' => $this->t('Check this box if you want to enable email rerouting. Uncheck to disable rerouting.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->configFactory()->getEditable('symfony_mailer_reroute.settings')
      ->set('enable', (bool) $form_state->getValue('enable'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
