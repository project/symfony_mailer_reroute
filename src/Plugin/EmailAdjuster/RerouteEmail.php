<?php

namespace Drupal\symfony_mailer_reroute\Plugin\EmailAdjuster;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Url;
use Drupal\symfony_mailer\Address;
use Drupal\symfony_mailer\EmailInterface;
use Drupal\symfony_mailer\Plugin\EmailAdjuster\AddressAdjusterBase;
use Drupal\symfony_mailer_reroute\RerouteCheckerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a Symfony Mailer Email Adjuster for rerouting emails.
 *
 * @EmailAdjuster(
 *   id = "reroute_email",
 *   label = @Translation("Reroute email (if enabled)"),
 *   description = @Translation("Reroutes emails send from the site to a predefined email. Useful for test sites."),
 *   provider = "symfony_mailer",
 *   weight = 999,
 * )
 */
class RerouteEmail extends AddressAdjusterBase implements ContainerFactoryPluginInterface, TrustedCallbackInterface {

  /**
   * The name of the associated header.
   */
  protected const NAME = 'To';

  /**
   * The symfony_mailer_reroute.settings config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The reroute checker service.
   *
   * @var \Drupal\symfony_mailer_reroute\RerouteCheckerInterface
   */
  protected $rerouteChecker;

  /**
   * The email validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The rerouting message to add to the email body.
   *
   * @var array
   */
  protected $reroutingMessage = [];

  /**
   * Constructs RerouteEmail plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\symfony_mailer_reroute\RerouteCheckerInterface $reroute_checker
   *   The reroute checker service.
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, RerouteCheckerInterface $reroute_checker, EmailValidatorInterface $email_validator, MessengerInterface $messenger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config_factory->get('symfony_mailer_reroute.settings');
    $this->rerouteChecker = $reroute_checker;
    $this->emailValidator = $email_validator;
    $this->messenger = $messenger;
    $this->reroutingMessage = [];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('symfony_mailer_reroute.checker'),
      $container->get('email.validator'),
      $container->get('messenger')
    );
  }

  /**
   * React to EmailInterface::PHASE_BUILD.
   *
   * @param \Drupal\symfony_mailer\EmailInterface $email
   *   The email to process.
   */
  public function build(EmailInterface $email) {
    if ($this->rerouteChecker->needsReroute($email, $this->getAllowList())) {
      // Save rerouting message, if it is configured to be added.
      if ((bool) $this->configuration['add_mail_description'] === TRUE) {
        $this->saveReroutingMessage($email);
      }

      // Call the parent adjuster plugin, to set the email To value to the
      // rerouted address(es).
      parent::build($email);

      // Show Drupal status message, if configured.
      if ((bool) $this->configuration['show_message'] === TRUE) {
        $this->messenger->addMessage($this->t('An email either aborted or rerouted to the configured address. Site administrators can check the recent log entries for complete details on the rerouted email. For more details please refer to Symfony Mailer Reroute settings.'));
      }
    }
  }

  /**
   * React to EmailInterface::PHASE_POST_RENDER.
   *
   * @param \Drupal\symfony_mailer\EmailInterface $email
   *   The email to process.
   */
  public function postRender(EmailInterface $email) {
    $email->setTextBody(implode(PHP_EOL, $this->reroutingMessage) . $email->getTextBody());
    $email->setHtmlBody(implode('<br />', $this->reroutingMessage) . $email->getHtmlBody());
  }

  /**
   * Save rerouting message to add to mail body, if configured.
   *
   * @param \Drupal\symfony_mailer\EmailInterface $email
   *   The email object to generate rerouting message for.
   */
  public function saveReroutingMessage(EmailInterface $email) {
    global $base_url;
    $this->reroutingMessage[] = $this->t('This email was rerouted.');
    $this->reroutingMessage[] = $this->t('Web site: @site', ['@site' => $base_url]);
    // @todo add mail key or identifier, as per reroute_email?
    $to_addresses = array_map(function (Address $address) {
      return $address->getEmail();
    }, $email->getTo());
    $this->reroutingMessage[] = $this->t('Originally to: @to', ['@to' => implode(', ', $to_addresses)]);
    // Add Cc/Bcc values to the message only if they are set.
    if ($cc_addresses = $email->getCc()) {
      $cc_addresses = array_map(function (Address $address) {
        return $address->getEmail();
      }, $cc_addresses);
      $this->reroutingMessage[] = $this->t('Originally cc: @cc', ['@cc' => implode(', ', $cc_addresses)]);
    }
    if ($bcc_addresses = $email->getBcc()) {
      $bcc_addresses = array_map(function (Address $address) {
        return $address->getEmail();
      }, $bcc_addresses);
      $this->reroutingMessage[] = $this->t('Originally bcc: @bcc', ['@bcc' => implode(', ', $bcc_addresses)]);
    }

    // Simple separator between reroute and original messages.
    $this->reroutingMessage[] = '-----------------------';
    $this->reroutingMessage[] = '';
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $form = parent::settingsForm($form, $form_state);

    // Alter original AddressAdjuster form to improve labels.
    $form['addresses']['#type'] = 'details';
    $form['addresses']['#title'] = $this->t('Reroute email to address(es)');
    $form['addresses']['#open'] = TRUE;

    // Show a warning if rerouting is not enabled.
    if ((bool) $this->config->get('enable') === FALSE) {
      $form['warning'] = [
        '#type' => 'item',
        '#title' => $this->t('Reroute email'),
        '#markup' => '<div class="messages messages--warning">' . $this->t('Make sure to <a href=":url">enable rerouting</a>, or the settings below will have no effect.', [':url' => Url::fromRoute('symfony_mailer_reroute.settings')->toString()]) . '</div>',
        '#weight' => -99,
      ];
    }

    // Add option form for the rerouting allowlist.
    $form['allowlist'] = [
      '#type' => 'textarea',
      '#rows' => 2,
      '#title' => $this->t('Skip email rerouting for email addresses:'),
      '#default_value' => $this->configuration['allowlist'] ?? '',
      '#description' => $this->t('Provide a line-delimited list of email addresses to pass through. All emails to addresses from this list will not be rerouted.<br/>A patterns like "*@example.com" and "myname+*@example.com" can be used to add all emails by its domain or the pattern.'),
      '#element_validate' => [
        [$this, 'validateMultipleEmails'],
        [$this, 'validateMultipleUnique'],
      ],
      '#pre_render' => [[$this, 'textareaRowsValue']],
    ];

    $form['show_message'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display a Drupal status message after rerouting'),
      '#default_value' => $this->configuration['show_message'] ?? TRUE,
      '#description' => $this->t('Check this box if you would like a Drupal status message to be displayed to users after submitting an email to let them know it was aborted to send or rerouted to a different email address.'),
    ];

    $form['add_mail_description'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show rerouting description in mail body'),
      '#default_value' => $this->configuration['add_mail_description'] ?? TRUE,
      '#description' => $this->t('Check this box if you want a message to be inserted into the email body when the mail is being rerouted. Otherwise, SMTP headers will be used to describe the rerouting. If sending rich-text email, leave this unchecked so that the body of the email will not be disturbed.'),
    ];

    return $form;
  }

  /**
   * Get allowlist configuration as array.
   *
   * @return array
   *   An array of unique email addresses for the allowlist.
   */
  public function getAllowList(): array {
    $allowlist = $this->configuration['allowlist'] ?? '';
    return $this->splitString($allowlist);
  }

  /**
   * Validate multiple email addresses field.
   *
   * @param array $element
   *   A field array to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateMultipleEmails(array $element, FormStateInterface $form_state): void {
    // Allow only valid email addresses.
    $element_key = ['config', 'reroute_email', 'allowlist'];
    $addresses = $this->splitString($form_state->getValue($element_key));
    foreach ($addresses as $address) {
      if (!$this->emailValidator->isValid($address)) {
        $form_state->setErrorByName($element['#name'], $this->t('@address is not a valid email address.', ['@address' => $address]));
      }
    }
  }

  /**
   * Validate multiple email addresses field.
   *
   * @param array $element
   *   A field array to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateMultipleUnique(array $element, FormStateInterface $form_state): void {
    // String "email@example.com; ;; , ,," save just as "email@example.com".
    // This will be ignored if any validation errors occur.
    $element_key = ['config', 'reroute_email', 'allowlist'];
    $form_state->setValue($element_key, implode(PHP_EOL, $this->splitString($form_state->getValue($element_key))));
  }

  /**
   * Split a string into an array by pre defined allowed delimiters.
   *
   * Items may be separated by any number and combination of:
   * spaces, commas, semicolons, or newlines.
   *
   * @param string|null $string
   *   A string to be split into an array.
   *
   * @return array
   *   An array of unique values from a string.
   */
  protected function splitString(?string $string): array {
    // Split string into array of emails.
    $addresses = preg_split('/[\s,;\n]+/', $string, -1, PREG_SPLIT_NO_EMPTY);

    // Remove duplications.
    $addresses = array_unique($addresses);

    // Make everything lowercased.
    return array_map('mb_strtolower', $addresses);
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['textareaRowsValue'];
  }

  /**
   * Adjust rows value according to the content size.
   *
   * @param array $element
   *   The render array.
   *
   * @return array
   *   The updated render array.
   */
  public static function textareaRowsValue(array $element): array {
    $size = substr_count($element['#default_value'], PHP_EOL) + 1;
    if ($size > $element['#rows']) {
      $element['#rows'] = min($size, 10);
    }
    return $element;
  }

}
