<?php

namespace Drupal\symfony_mailer_reroute;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\symfony_mailer\EmailInterface;

/**
 * Service to check whether email should be rerouted.
 */
class RerouteChecker implements RerouteCheckerInterface {

  /**
   * The email validator service.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * The symfony_mailer_reroute config.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs RerouteChecker service.
   *
   * @param \Drupal\Component\Utility\EmailValidatorInterface $email_validator
   *   The email validator service.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EmailValidatorInterface $email_validator, ConfigFactory $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->emailValidator = $email_validator;
    $this->config = $config_factory->get('symfony_mailer_reroute.settings');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function needsReroute(EmailInterface $email, array $allow_list = []): bool {
    // Check if rerouting is enabled globally.
    if ((bool) $this->config->get('enable') === FALSE) {
      return FALSE;
    }

    // Get original email addresses.
    $original_addresses = $email->getTo();
    if ($email->getCc()) {
      $original_addresses = array_merge($original_addresses, $email->getCc());
    }
    if ($email->getBcc()) {
      $original_addresses = array_merge($original_addresses, $email->getBcc());
    }

    // Split allowed domains and partial addresses patterns from the allowlist.
    $allowlist_patterns = [];
    foreach ($allow_list as $key => $allow_list_email) {
      if (substr_count($allow_list_email, '*') > 0) {
        $allow_list_email = '/^' . preg_quote($allow_list_email, '/') . '$/';
        $allowlist_patterns[$allow_list_email] = str_replace('\*', '[^@]+', $allow_list_email);
        unset($allow_list[$key]);
      }
    }

    // Compare original addresses with the allow list.
    $invalid = 0;
    foreach ($original_addresses as $original_address) {

      // Just ignore all invalid email addresses.
      if ($this->emailValidator->isValid($original_address->getEmail()) === FALSE) {
        $invalid++;
        continue;
      }

      // Check email in the allow list.
      if (in_array($original_address->getEmail(), $allow_list, TRUE)) {
        $email->addTextHeader('X-Rerouted-Reason', 'ALLOWLISTED');
        continue;
      }
      // Check allowed domains and partial address patterns from the allowlist.
      foreach ($allowlist_patterns as $pattern) {
        if (preg_match($pattern, $original_address->getEmail())) {
          $email->addTextHeader('X-Rerouted-Reason', 'PATTERN');
          continue 2;
        }
      }

      // @todo implement functionality like in reroute_mail.
      // Check users by roles.
      // if ($this->checkEmailRole($original_address->getEmail())) {
      //  $email->addTextHeader('X-Rerouted-Reason', 'ROLE');
      //  continue;
      // }

      // No need to continue if at least one address should be rerouted.
      $email->addTextHeader('X-Rerouted-Status', 'REROUTED');
      return TRUE;
    }

    // Reroute if all addresses are invalid.
    if (count($original_addresses) === $invalid) {
      $email->addTextHeader('X-Rerouted-Reason', 'INVALID-ADDRESSES');
      $email->addTextHeader('X-Rerouted-Status', 'REROUTED');
      return TRUE;
    }

    // All email addresses are in the allowed list.
    $email->addTextHeader('X-Rerouted-Status', 'NOT-REROUTED');
    return FALSE;
  }

}
