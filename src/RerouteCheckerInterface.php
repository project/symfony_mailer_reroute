<?php

namespace Drupal\symfony_mailer_reroute;

use Drupal\symfony_mailer\EmailInterface;

/**
 * Interface for RerouteChecker service.
 */
interface RerouteCheckerInterface {

  /**
   * Check if given email needs reroute.
   *
   * @param \Drupal\symfony_mailer\EmailInterface $email
   *   The email object to check.
   * @param array $allow_list
   *   The list of allowed email addresses that should not be rerouted.
   *
   * @return bool
   *   Whether the given email needs to be rerouted.
   */
  public function needsReroute(EmailInterface $email, array $allow_list = []): bool;

}
